

const controller = {}

const Product = require('./model')

// const jwt = require('jsonwebtoken')

const mid = require('./middleware')

const { getPosData } = require('../../utils')

controller.signup = async (req, res, next) => {

    try {
        // const body = await getPosData(req)

        // console.log(body)
        const { firstname, lastname, emailId, password } = req.body

        const product = {
            firstname,
            lastname,
            emailId,
            password
        }
        const newProduct = await Product.create(product)

        if (newProduct) {

            console.log(newProduct)
            // res.writeHead(201, { 'Content-Type': 'applciation/json' })

            return res.json(newProduct)
        }
        else {
            return res.json({ message: 'this email already exist' })
        }

    } catch (error) {
        console.log(error)
    }

}

controller.login = async (req, res, next) => {

    try {
        // const body = await getPosData(req)

        const { firstname, lastname, emailId, password } = req.body

        const product = {
            firstname,
            lastname,
            emailId,
            password
        }


        console.log(product)

        const matchproduct = await Product.findById(product)
        console.log(matchproduct)

        if (!matchproduct) {
            console.log("not match")
            return res.json({ message: 'product not found' })
        } else {
            const token = await mid.generateAuthToken(matchproduct)

            return res.json({ "jwt_token": "bearer " + token })
        }

    } catch (error) {
        console.log(error)
    }

}

controller.getOrder = async (req, res, next) => {



    try {
        const body = await getPosData(req)

        const item = JSON.parse(body)

        const matchproduct = await Product.findByOrderdesc(item)
        if (matchproduct) {
            console.log("match")

            return res.json((matchproduct))
        } else {
            console.log("not match")

            return res.json(({ message: 'product out of stocks' }))
        }
    } catch (error) {
        console.log(error)
    }

}


controller.changePassword = async (req, res, next) => {
    try {
        const body = await getPosData(req)
        const item = JSON.parse(body)

        const change = await Product.userChangePassword(item)

        if (change.status) {

            return res.json(({ message: change.message }))
        } else {
            res.writeHead(404, { 'Content-Type': 'application/json' })
            return res.json(({ message: change.message }))
        }

    } catch (error) {
        console.log(error)
        // return res.send(JSON.stringify({ message: error }))
    }
}

// ?.


controller.getALL = async (req, res, next) => {

    try {
        const change = await Product.findAll()

        console.log(change)

        return res.json({ message: change })
    } catch (error) {
        console.log(error)
    }
}



controller.profileedit = async (req, res, next) => {

    console.log(req.user + "vdjvhj")
    const ans = await mid.verifyToken(req.user)
    console.log(ans)
    // if (ans) {
    console.log("nnkjjbjbbm,")
    try {
        const body = await getPosData(req)
        const item = JSON.parse(body)
        console.log(item)

        const change = await Product.editProfile(item)

        console.log(change)

        return res.send(JSON.stringify({ message: "change.message " }))

    } catch (error) {
        console.log(error)
        // return res.send(JSON.stringify({ message: error }))
    }
    // } else {
    //     res.send()
    // }


}





module.exports = controller 