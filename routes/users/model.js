let products = require('../../user.json');
let orders = require('../../order.json');
const { v4: uuidv4 } = require('uuid')

const { writeDataToFile } = require('../../utils');



const model = {}

model.findAll = () => {
    return new Promise((resolve, reject) => {
        resolve(products)
    })
}


model.create = (product) => {
    console.log(uuidv4())
    return new Promise((resolve, reject) => {

        const matchproduct = products.findIndex((p) => p.emailId === product.emailId)
        if (matchproduct < 0) {

            const newProduct = { id: uuidv4(), ...product }
            products.push(newProduct)
            writeDataToFile('./user.json', products)
            resolve(newProduct)
        } else {
            resolve(false)
        }
    })
}


model.findById = async (id) => {
    console.log(uuidv4)

    try {
        const product = await products.find((p) => (p.emailId === id.emailId) && (p.password === id.password))
        return product
    } catch (error) {
        return (error.message)
    }

    // return new Promise((resolve, reject) => {
    //     const product = products.find((p) => (p.emailId === id.emailId) && (p.password === id.password))
    //     resolve(product)
    // })
}


model.userChangePassword = (id) => {
    // console.log(uuidv4)
    return new Promise((resolve, reject) => {
        const product = products.find((p) => (p.emailId === id.emailId))
        if (product) {
            if (product.password == id.password) {
                if (id.newpassword == id.confirmpassword) {
                    const changesuccess = products.map(p =>
                        p.emailId === id.emailId
                            ? { ...p, password: id.newpassword }
                            : p
                    );
                    writeDataToFile('./user.json', changesuccess)
                    console.log(changesuccess)
                    resolve({ message: id.emailId + " you password change successfully", status: true })
                }
                resolve({ message: "newpassworsd and confirm passsword should be same", status: false })
            } else {
                resolve({ message: "passworsd not match", status: false })
            }
        } else {
            resolve({ message: "user not match", status: false })
        }
    })
}

model.findByOrderdesc = (id) => {
    console.log(id)
    // console.log(orders)
    return new Promise((resolve, reject) => {
        const order = orders.find((p) => (p.item === id.item))
        console.log(order)

        if (order.quantity > 0) {
            console.log(order.quantity)
            const newProjects = orders.map(p =>
                p.item === id.item
                    ? { ...p, quantity: p.quantity - 1 }
                    : p
            );
            writeDataToFile('./order.json', newProjects)
            console.log(newProjects)
            resolve(id.item + " you purchase")
        } else {
            resolve(false)
        }
    })
}


model.editProfile = (id) => {
    return new Promise((resolve, reject) => {
        const product = products.find((p) => (p.emailId === id.emailId))
        console.log(product)

        const { firstname, lastname, emailId } = id

        console.log(firstname)

        //this below function take a place of patch
        console.log(product.firstname)

        const Data = {
            firstname: firstname ?? product.firstname,
            lastname: lastname ?? product.lastname,
            emailId: emailId ?? product.emailId
        }
        console.log(Data)

        if (product) {
            const changesuccess = products.map(p =>
                p.emailId === id.emailId
                    ? { ...p, ...Data }
                    : p
            );
            writeDataToFile('./user.json', changesuccess)
            // console.log(newProjects)
            resolve(id.emailId + " change to " + Data.emailId)
        } else {
            resolve(false)
        }
    })
}








module.exports = model