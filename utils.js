const fs = require('fs');

function writeDataToFile(filename, content) {
    fs.writeFile(filename, JSON.stringify(content), 'utf8', (err) => {
        console.log("Data")
        if (err) {
            console.log(err)
        }
    })
}


function getPosData(req) {
    return new Promise((resolve, request) => {
        try {
            let body = "";
            req.on('data', (chunk) => {
                body += chunk.toString()
            })
            req.on('end', () => {
                console.log(body)
                resolve(body)
            })
        } catch (error) {
        }
    })
}



module.exports = {
    writeDataToFile,
    getPosData
}