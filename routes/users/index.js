const router = require('express').Router();

//import controller
const controller = require('./controller')
const middleware = require('./middleware')
const validator = require('./validator')

//const { body, validationResult } = require('express-validator');




router.post('/signup', controller.signup)

router.post('/login', controller.login)

router.post('/editprofile', middleware.authenticateToken, controller.profileedit)

router.post('/changepassword', controller.changePassword)

router.post('/order', middleware.authenticateToken, controller.getOrder)

router.get('/list', controller.getALL)

router.get('/give/me', middleware.authenticateToken, async (req, res) => {
    res.send(req.user)
})

router.all('*', async (req, res) => {
    try {
        res.status(404).send("not found eouter")
    } catch (error) {
        res.status(404).send(error.message)
    }
})


module.exports = router