const express = require('express')

require('dotenv').config()

const bodyparser = require('body-parser')

const app = express();

app.use(express.json());
app.use(bodyparser.urlencoded({ extended: true }));

//import router file
const route = require('./routes/route');


app.use('/api', route)



app.listen(process.env.PORT, () => {
    console.log(`server listing on port ${process.env.PORT}`)
})
